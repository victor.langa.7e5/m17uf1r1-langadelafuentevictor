using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    private PlayerMovementControlled _controlledPlayerMovement;
    private PlayerMovementAutonomous _autonomousPlayerMovement;

    private float _speed;
    private float _lastHorizontalPosition;
    private int _movementSense = 1;

    private void Start()
    {
        _controlledPlayerMovement = GetComponent<PlayerMovementControlled>();
        _autonomousPlayerMovement = GetComponent<PlayerMovementAutonomous>();

        _speed = 300;
    }

    private void Update() => FacePlayerBasedOnSense();

    private void FacePlayerBasedOnSense()
    {
        switch (_movementSense)
        {
            case 1:
                _movementSense *= ChangeRotationIfSenseChanged(_lastHorizontalPosition, transform.position.x);
                break;
            case -1:
                _movementSense *= ChangeRotationIfSenseChanged(transform.position.x, _lastHorizontalPosition);
                break;
        }

        _lastHorizontalPosition = transform.position.x;
    }

    private int ChangeRotationIfSenseChanged(float position1, float position2)
    {
        if (position1 - position2 > 0.01f)
        {
            transform.Rotate(0, 180, 0);
            return -1;
        }

        return 1;
    }
    public void SetMovementMode(bool isControlled)
    {
        switch (isControlled)
        {
            case true:
                _controlledPlayerMovement.enabled = true;
                break;
            case false:
                _autonomousPlayerMovement.enabled = true;
                break;
        }
    }

    public void Die() => GetComponent<PlayerAnimation>().SetDie();

    public void Restart()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0);
        GetComponent<PlayerAnimation>().Restart();
    }

    public int GetMovementSense() => _movementSense;

    public float GetSpeed() => _speed;
}