using UnityEngine;

public class FireBallAnimation : MonoBehaviour
{
    private Animator _animator;
    
    private void Start() => _animator = GetComponent<Animator>();

    public void TriggerExplosion() => _animator.SetTrigger("Explosion");
}
