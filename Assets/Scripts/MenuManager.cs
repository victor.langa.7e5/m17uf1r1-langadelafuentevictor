using TMPro;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    private Transform _playerTransform;
    private Transform _backgroundTransform;

    [SerializeField]
    private TMP_InputField _playerName;

    private struct UpAndDownAttributes 
    {
        public int sense;
        public float range;
        public float currentRangePosition;
        public float velocity;
    }

    private UpAndDownAttributes _playerAttributes;
    private UpAndDownAttributes _backgroundAttributes;

    private void Start()
    {
        _playerTransform = GameObject.Find("Player").transform;
        _playerAttributes.sense = -1;
        _playerAttributes.range = 1;
        _playerAttributes.currentRangePosition = _playerTransform.position.y;

        _backgroundTransform = GameObject.Find("Background").transform;
        _backgroundAttributes.sense = 1;
        _backgroundAttributes.range = 1;
        _backgroundAttributes.currentRangePosition = _backgroundTransform.position.y;
    }

    private void Update()
    {
        _playerAttributes = MoveUpAndDown(_playerTransform, _playerAttributes);
        _backgroundAttributes = MoveUpAndDown(_backgroundTransform, _backgroundAttributes);
    }

    private void OnDestroy() => PlayerPrefs.SetString("PlayerName", string.IsNullOrEmpty(_playerName.text) ? "HeroKnight" : _playerName.text);

    private UpAndDownAttributes MoveUpAndDown(Transform transform, UpAndDownAttributes attributes)
    {
        if (Mathf.Abs(attributes.currentRangePosition) - attributes.range > attributes.range)
            attributes.sense *= -1;

        attributes.velocity = Mathf.Clamp(attributes.range - Mathf.Abs(attributes.currentRangePosition), 0.3f, attributes.range - Mathf.Abs(attributes.currentRangePosition));
        attributes.currentRangePosition += attributes.velocity * attributes.sense * Time.deltaTime;
        transform.position = new Vector3(transform.position.x, attributes.currentRangePosition);

        return attributes;
    }
}
