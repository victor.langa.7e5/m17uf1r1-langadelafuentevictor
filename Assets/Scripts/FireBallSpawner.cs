using UnityEngine;

public class FireBallSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject _fireBall;
    [SerializeField]
    private GameObject _goodMushroom;
    [SerializeField]
    private GameObject _toxicMushroom;

    private Transform _playerTransform;

    private void Start() => _playerTransform = GameObject.Find("Player").transform;

    private void Update() => transform.position = new Vector3(_playerTransform.position.x, transform.position.y, transform.position.z);

    public void SpawnObject(Vector3 position)
    {
        GameObject objectToSpawn = _fireBall;
        switch (Random.Range(0, 100))
        {
            case > 95:
                switch (GameManager.Instance.GetMode())
                {
                    case "ControlPlayer_AutonomousFireBall":
                        objectToSpawn = _toxicMushroom;
                        break;
                    case "AutonomousPlayer_ControlFireBall":
                        objectToSpawn = _goodMushroom;
                        break;
                }
                break;

        }
        Instantiate(objectToSpawn, position, Quaternion.identity, transform.parent);
    }
}
