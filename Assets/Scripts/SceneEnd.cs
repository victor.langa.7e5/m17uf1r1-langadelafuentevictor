using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneEnd : MonoBehaviour
{
    private Image _image;
    private Color _imageColor;
    private float _alpha;
    public string _sceneToLoad;

    private void Start()
    {
        _image = GetComponent<Image>();
        _imageColor = _image.color;
        _alpha = 0;
    }

    private void Update()
    {
        _alpha += 0.5f * Time.deltaTime;
        _imageColor.a = _alpha;
        _image.color = _imageColor;
        if (_alpha >= 1) LoadNextScene();
    }

    public void GetSceneToLoad(string sceneToLoad) => _sceneToLoad = sceneToLoad;

    private void LoadNextScene()
    {
        switch (_sceneToLoad)
        {
            case "Menu":
                SceneManager.LoadScene("Menu");
                break;
            case "Game":
                SceneManager.LoadScene("Game");
                break;
            case "Exit":
                Application.Quit();
                break;
        }
    }
}