using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    private Animator _animator;

    private void Start() => _animator = GetComponent<Animator>();

    public void SetAnimationValues(float velocityX, float velocityY, bool grounded)
    {
        SetRun(Mathf.Abs(velocityX));
        switch (grounded)
        {
            case true:
                SetVerticalSpeed(0);
                break;
            case false:
                SetVerticalSpeed(velocityY);
                break;
        }
    }

    public void SetVerticalSpeed(float verticalSpeed) => _animator.SetFloat("VerticalSpeed", verticalSpeed);

    public void SetRun(float horizontalSpeed) => _animator.SetFloat("Run", horizontalSpeed);

    public void SetDie() => _animator.SetBool("Death", true);

    public void Restart() => _animator.SetBool("Death", false);
}
