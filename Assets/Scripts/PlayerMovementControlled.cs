using UnityEngine;

public class PlayerMovementControlled : MonoBehaviour
{
    private Player _player;
    private PlayerAnimation _playerAnimation;
    private IsGrounded _isGrounded;
    private Rigidbody2D _rigidbody;


    private void Start()
    {
        _player = GetComponent<Player>();
        _playerAnimation = GetComponent<PlayerAnimation>();
        _isGrounded = GetComponent<IsGrounded>();
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update() => _playerAnimation.SetAnimationValues(_rigidbody.velocity.x, _rigidbody.velocity.y, _isGrounded.GetIfHalfGrounded());

    private void FixedUpdate()
    {
        if (_isGrounded.GetIfHalfGrounded())
        {
            MoveGround();
            Jump();
        }
        else MoveJump();
    }

    private void MoveGround() =>_rigidbody.velocity = new Vector2(Input.GetAxis("Horizontal") * _player.GetSpeed() * Time.deltaTime, _rigidbody.velocity.y);

    private void MoveJump() 
    {
        if (Mathf.Abs(_rigidbody.velocity.x) < _player.GetSpeed() * Time.deltaTime)
            _rigidbody.velocity += new Vector2(Input.GetAxis("Horizontal") * _player.GetSpeed() * Time.deltaTime * 0.02f, 0);
    }

    private void Jump()
    {
        if (Input.GetKey(KeyCode.Space) && _isGrounded.GetIfHalfGrounded())
            _rigidbody.AddForce(new Vector2(0, 2750 * Time.deltaTime));
    }
}
