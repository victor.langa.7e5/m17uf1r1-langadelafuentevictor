using UnityEngine;

public class Mushroom : MonoBehaviour
{
    private SpriteRenderer _spriteRenderer;
    private float _timer;
    private float _timeSinceLastBeep;

    private void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _timer = 0;
        _timeSinceLastBeep = 0;
    }

    void Update()
    {
        _timeSinceLastBeep += Time.deltaTime;
        _timer += Time.deltaTime;
        if (_timer > 7.5f)
            Destroy(gameObject);
        if (_timer > 5 && _timeSinceLastBeep > 0.25f)
        {
            switch (_spriteRenderer.enabled)
            {
                case true:
                    _spriteRenderer.enabled = false;
                    break;
                case false:
                    _spriteRenderer.enabled = true;
                    break;
            }
            _timeSinceLastBeep = 0;
        }

    }
}
