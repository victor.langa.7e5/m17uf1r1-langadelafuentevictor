using UnityEngine;

public class FireBall : MonoBehaviour
{
    private FireBallAnimation _fireBallAnimation;

    private void Start() => _fireBallAnimation = GetComponent<FireBallAnimation>();

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "FireBall":
                break;
            case "Death":
                Destroy(gameObject);
                break;
            default:
                Destroy(GetComponent<Rigidbody2D>());
                Destroy(GetComponent<Collider2D>());
                if (collision.gameObject.tag == "Player")
                    GameManager.Instance.TriggerPlayerDeath();
                _fireBallAnimation.TriggerExplosion();
                break;
        }
    }
}
