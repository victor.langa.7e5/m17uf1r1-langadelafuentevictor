using System.Collections;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance { get => _instance; }

    [SerializeField]
    private GameObject _fireball;
    [SerializeField]
    private GameObject _whiteFadeInOut;

    private float _timer;
    private string _mode;
    private bool _isGameRunning;

    private void Awake()
    {
        if (_instance != null && _instance != this) Destroy(this);
        _instance = this;
    }

    private void Start() => _isGameRunning = false;

    private void Update()
    {
        _timer -= Time.deltaTime;

        if (_timer < 2 && _isGameRunning)
            DeactivateFireBallSpawner();

        if (_timer < 0 && _isGameRunning)
        {
            _isGameRunning = false;
            TriggerPlayerVictory();
        }
    }

    public void SetMode(string mode) => _mode = mode;

    public void StartGame()
    {
        if (_mode is null) return;
        _isGameRunning = true;

        Destroy(GameObject.Find("StartMenu"));

        GameObject player = GameObject.Find("Player");
        GameObject fireBallSpawner = GameObject.Find("FireBallSpawner");

        switch (_mode)
        {
            case "ControlPlayer_AutonomousFireBall":
                player.GetComponent<PlayerMovementControlled>().enabled = true;
                fireBallSpawner.GetComponent<FireBallSpawnAutonomous>().enabled = true;
                GameObject.Find("Main Camera").GetComponent<CameraManager>().enabled = true;
                break;
            case "AutonomousPlayer_ControlFireBall":
                player.GetComponent<PlayerMovementAutonomous>().enabled = true;
                fireBallSpawner.GetComponent<FireBallSpawnControlled>().enabled = true;
                break;
        }

        Destroy(GameObject.Find("BeforeStartPlayerPlatform"));
    }

    private void TriggerPlayerVictory()
    {
        PlayerAnimation playerAnimation = GameObject.Find("Player").GetComponent<PlayerAnimation>();
        TriggerEndGameCommons();
        GameObject.Find("Player").GetComponent<Rigidbody2D>().gravityScale = 0;
        playerAnimation.SetRun(0);
        playerAnimation.SetVerticalSpeed(0);

        EndGameVictoryCinematic();

        StartCoroutine(EndGame(true));
    }

    public void TriggerPlayerDeath()
    {
        if (_isGameRunning)
        {
            _isGameRunning = false;

            GameObject.Find("Main Camera").GetComponent<CameraManager>().enabled = false;

            DeactivateFireBallSpawner();
            TriggerEndGameCommons().GetComponent<Player>().Die();

            StartCoroutine(EndGame(false));
        }
    }

    private GameObject TriggerEndGameCommons()
    {
        GetComponent<UIManager>().SetTimerInactive();

        GameObject player = GameObject.Find("Player");
        player.GetComponent<PlayerMovementControlled>().enabled = false;
        player.GetComponent<PlayerMovementAutonomous>().enabled = false;

        return player;
    }

    private void DeactivateFireBallSpawner()
    {
        GameObject fireBallSpawner = GameObject.Find("FireBallSpawner");
        fireBallSpawner.GetComponent<FireBallSpawnControlled>().enabled = false;
        fireBallSpawner.GetComponent<FireBallSpawnAutonomous>().enabled = false;
    }

    private IEnumerator EndGame(bool isVictory)
    {
        yield return new WaitForSecondsRealtime(2);

        switch (isVictory, _mode)
        {
            case (true, "ControlPlayer_AutonomousFireBall"):
            case (false, "AutonomousPlayer_ControlFireBall"):
                GameObject victoryPanel = GameObject.Find("Canvas").transform.Find("VictoryPanel").gameObject;
                victoryPanel.SetActive(true);
                victoryPanel.transform.Find("PlayerWonText").GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetString("PlayerName") + " has achieved victory!";
                break;
            case (true, "AutonomousPlayer_ControlFireBall"):
            case (false, "ControlPlayer_AutonomousFireBall"):
                GameObject deathPanel = GameObject.Find("Canvas").transform.Find("DeathPanel").gameObject;
                deathPanel.SetActive(true);
                deathPanel.transform.Find("PlayerDiedText").GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetString("PlayerName") + " has been defeated...";
                break;
        }

        GameObject.Find("Canvas").transform.Find("EndGameButtonsPanel").gameObject.SetActive(true);
    }

    public void RestartGame()
    {
        GameObject player = GameObject.Find("Player");
        player.GetComponent<Player>().Restart();
        player.GetComponent<GiantState>().ResetGiantState();
        player.GetComponent<Rigidbody2D>().gravityScale = 3;
        player.transform.position = new Vector3(0, 6.33f);
        GameObject.Find("Main Camera").GetComponent<Camera>().transform.position = new Vector3(0, 0, -10);
        GameObject.Find("Canvas").transform.Find("VictoryPanel").gameObject.SetActive(false);
        GameObject.Find("Canvas").transform.Find("DeathPanel").gameObject.SetActive(false);
        GameObject.Find("Canvas").transform.Find("EndGameButtonsPanel").gameObject.SetActive(false);
        StartGame();
    }

    public void EndGameVictoryCinematic()
    {
        Instantiate(_whiteFadeInOut, GameObject.Find("Canvas").transform).GetComponent<WhiteFadeInOut>().timing = 11;

        GameObject player = GameObject.Find("Player");
        player.transform.position = new Vector2(0, -2.9f);

        Camera camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        camera.GetComponent<CameraManager>().enabled = false;
        camera.transform.position = new Vector3(0, 0, camera.transform.position.z);

        GameObject fireBall = Instantiate(_fireball);
        fireBall.transform.position = new Vector2(0, 8);
        fireBall.transform.localScale = new Vector2(6, 6);

        Instantiate(_whiteFadeInOut, GameObject.Find("Canvas").transform).GetComponent<WhiteFadeInOut>().timing = 0.7f;

        player.GetComponent<Animator>().SetTrigger("Attack");

        Rigidbody2D rigidbody = player.GetComponent<Rigidbody2D>();
        rigidbody.velocity = Vector2.zero;
        rigidbody.AddForce(new Vector2(0, 70));
    }

    public void SetTimer(int timer) => _timer = timer;

    public float GetTimer() => _timer;

    public string GetMode() => _mode;
}
