using UnityEngine;

public class LevelsManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _level1;
    [SerializeField]
    private GameObject _level2;
    [SerializeField]
    private GameObject _level3;

    public enum Levels
    { 
        Level1,
        Level2,
        Level3
    }

    private Levels _spawnedLevel;

    private void Start() => _spawnedLevel = Levels.Level1;



    public void SpawnNewLevel(int levelNumber) 
    {
        Levels level = Levels.Level1;
        switch (levelNumber)
        {
            case 2:
                level = Levels.Level2;
                break;
            case 3:
                level = Levels.Level3;
                break;
        }

        if (level != _spawnedLevel)
            DespawnLevel();

        switch (level)
        {
            case Levels.Level1:
                if (_spawnedLevel != Levels.Level1)
                {
                    Instantiate(_level1);
                    _spawnedLevel = Levels.Level1;
                }
                break;
            case Levels.Level2:
                if (_spawnedLevel != Levels.Level2)
                {
                    Instantiate(_level2);
                    _spawnedLevel = Levels.Level2;
                }
                break;
            case Levels.Level3:
                if (_spawnedLevel != Levels.Level3)
                {
                    Instantiate(_level3);
                    _spawnedLevel = Levels.Level3;
                }
                break;
        }
    }

    private void DespawnLevel()
    {
        switch (_spawnedLevel)
        {
            case Levels.Level1:
                Destroy(GameObject.Find("Level1(Clone)"));
                break;
            case Levels.Level2:
                Destroy(GameObject.Find("Level2(Clone)"));
                break;
            case Levels.Level3:
                Destroy(GameObject.Find("Level3(Clone)"));
                break;
        }
    }

}
