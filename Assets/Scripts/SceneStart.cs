using UnityEngine;
using UnityEngine.UI;

public class SceneStart : MonoBehaviour
{
    private Image _image;
    private Color _imageColor;
    private float _alpha;

    private void Start()
    {
        _image = GetComponent<Image>();
        _imageColor = _image.color;
        _alpha = 1;
    }

    private void Update()
    {
        _alpha -= 0.5f * Time.deltaTime;
        _imageColor.a = _alpha;
        _image.color = _imageColor;
        if (_alpha <= 0) Destroy(gameObject);
    }
}
