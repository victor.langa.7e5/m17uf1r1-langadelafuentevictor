using UnityEngine;

public class PlayerMovementAutonomous : MonoBehaviour
{
    private Player _player;
    private PlayerAnimation _playerAnimation;
    private IsGrounded _isGrounded;
    private Rigidbody2D _rigidbody;
    private Collider2D _collider;

    private int _speedSense;
    private RaycastHit2D _hit;
    private float _timeSinceLastSpeedSenseChanged;

    private void Start()
    {
        _player = GetComponent<Player>();
        _playerAnimation = GetComponent<PlayerAnimation>();
        _isGrounded = GetComponent<IsGrounded>();
        _rigidbody = GetComponent<Rigidbody2D>();
        _collider = GetComponent<Collider2D>();

        _speedSense = _player.GetMovementSense();
        _timeSinceLastSpeedSenseChanged = 0;
    }

    private void Update()
    {
        _timeSinceLastSpeedSenseChanged += Time.deltaTime;
        _playerAnimation.SetAnimationValues(_rigidbody.velocity.x, _rigidbody.velocity.y, _isGrounded.GetIfHalfGrounded());

        if (!_isGrounded.GetIfTotalGrounded() && _timeSinceLastSpeedSenseChanged > 0.1)
        {
            _speedSense *= -1;
            _timeSinceLastSpeedSenseChanged = 0;
        }
    }

    private void FixedUpdate()
    {
        AvoidFireBall();
        if (_isGrounded.GetIfHalfGrounded())
            MoveGround();
    }

    private void MoveGround() => _rigidbody.velocity = new Vector2(_player.GetSpeed() * _speedSense * Time.deltaTime, _rigidbody.velocity.y);

    private void AvoidFireBall()
    {
        _hit = Physics2D.Raycast(new Vector2(_collider.bounds.center.x, _collider.bounds.max.y), Vector3.right * _speedSense, 1f);
        if (_hit.collider?.gameObject.tag == "FireBall" || _hit.collider?.gameObject.tag == "Ground")
            _speedSense *= -1;
    }
}
