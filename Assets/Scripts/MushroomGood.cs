using UnityEngine;

public class MushroomGood : Mushroom
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<Invulnerability>().enabled = true;
            Destroy(gameObject);
        }
    }
}
