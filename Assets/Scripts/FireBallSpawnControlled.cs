using UnityEngine;

public class FireBallSpawnControlled : MonoBehaviour
{
    [SerializeField]
    private GameObject _prefabAvailableFireBall;

    private GameObject _availableFireBall;
    private FireBallSpawner _fireBallSpawner;
    private Camera _camera;

    private float _cooldown;

    private void Start()
    {
        _fireBallSpawner = GetComponent<FireBallSpawner>();
        _camera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    private void OnEnable()
    {
        _availableFireBall = Instantiate(_prefabAvailableFireBall, GameObject.Find("Canvas").transform);
        _cooldown = 0;
    }

    private void OnDisable() => Destroy(_availableFireBall);

    private void Update()
    {
        _cooldown += Time.deltaTime;
        if (_cooldown > GameManager.Instance.GetTimer() / 30)
        {
            _availableFireBall.SetActive(true);
            if (Input.GetMouseButtonDown(0))
            {
                _fireBallSpawner.SpawnObject(new Vector3(_camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, 0)).x, _fireBallSpawner.transform.position.y));
                _cooldown = 0;
                _availableFireBall.SetActive(false);
            }
        }
    }

}
