using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    private TextMeshProUGUI _timerText;

    private void Start() => _timerText = GameObject.Find("Canvas").transform.Find("Timer").GetComponent<TextMeshProUGUI>();

    private void Update() => _timerText.text = ((int)GameManager.Instance.GetTimer()).ToString();

    public void SetTimerActive()
    {
        if (GameManager.Instance.GetMode() != null)
        {
            GameManager.Instance.SetTimer(30);
            _timerText.gameObject.SetActive(true);
        }
    }

    public void SetTimerInactive() => _timerText.gameObject.SetActive(false); 
}
