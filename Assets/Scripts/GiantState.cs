using UnityEngine;

public class GiantState : MonoBehaviour
{
    private enum GiantStateEnum
    { 
        NotActive,
        ProcState,
        Active,
        End
    }

    private GiantStateEnum _giantState;
    private float _giantStateTimer;

    void Start()
    {
        _giantState = GiantStateEnum.NotActive;
        _giantStateTimer = 0;
    }

    void Update()
    {
        if (_giantState != GiantStateEnum.NotActive)
            UpdateGiantState();
    }

    private void UpdateGiantState()
    {
        switch (_giantState)
        {
            case GiantStateEnum.ProcState:
                _giantStateTimer += Time.deltaTime;
                if (_giantStateTimer < 1)
                    transform.localScale += new Vector3(Time.deltaTime, Time.deltaTime);
                else
                {
                    _giantStateTimer = 0;
                    _giantState = GiantStateEnum.Active;
                }
                break;
            case GiantStateEnum.Active:
                _giantStateTimer += Time.deltaTime;
                if (_giantStateTimer >= 3)
                {
                    _giantStateTimer = 0;
                    _giantState = GiantStateEnum.End;
                }
                break;
            case GiantStateEnum.End:
                _giantStateTimer += Time.deltaTime;
                if (_giantStateTimer < 1)
                    transform.localScale -= new Vector3(Time.deltaTime, Time.deltaTime);
                else
                { 
                    _giantState = GiantStateEnum.NotActive;
                    _giantStateTimer = 0;
                }
                break;
        }
    }

    public void ProcGiantState() => _giantState = GiantStateEnum.ProcState;

    public void ResetGiantState()
    {
        _giantState = GiantStateEnum.NotActive;
        transform.localScale = new Vector2(0.8002f, 0.7162f);
        _giantStateTimer = 0;
    }
}
