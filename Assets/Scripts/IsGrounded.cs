using UnityEngine;

public class IsGrounded : MonoBehaviour
{
    private RaycastHit2D _groundedLeft;
    private RaycastHit2D _groundedRight;
    private Collider2D _collider;

    private void Start() => _collider = GetComponent<Collider2D>();

    private void FixedUpdate() 
    {
        _groundedLeft = Physics2D.Raycast(_collider.bounds.min, Vector2.down, 0.5f);
        _groundedRight = Physics2D.Raycast(new Vector2(_collider.bounds.max.x, _collider.bounds.min.y), Vector2.down, 0.5f);
    }

    public bool GetIfHalfGrounded() => _groundedLeft.collider?.tag == "Ground" || _groundedRight.collider?.tag == "Ground";

    public bool GetIfTotalGrounded() => _groundedLeft.collider?.tag == "Ground" && _groundedRight.collider?.tag == "Ground";
}
