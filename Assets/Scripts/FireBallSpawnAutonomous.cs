using UnityEngine;

public class FireBallSpawnAutonomous : MonoBehaviour
{
    private FireBallSpawner _fireBallSpawner;

    public float minSpawnRange;
    public float maxSpawnRange;

    public bool ignoreTimer;
    private float _timer;
    private float _spawnerTimer;

    private void Start()
    {
        _fireBallSpawner = GetComponent<FireBallSpawner>();
        _timer = 5;
        _spawnerTimer = 0;
    }

    private void Update()
    {
        if (!ignoreTimer) _timer = (int)GameManager.Instance.GetTimer();

        _spawnerTimer += Time.deltaTime;
        if (_timer / 30 < _spawnerTimer)
        {
            _spawnerTimer = 0;
            GenerateFireBall();
        }
    }

    private void GenerateFireBall() => _fireBallSpawner.SpawnObject(new Vector3(transform.position.x + Random.Range(minSpawnRange, maxSpawnRange), transform.position.y));
}
