using UnityEngine;
using UnityEngine.UI;

public class WhiteFadeInOut : MonoBehaviour
{
    public float timing;

    private Image _image;
    private Color _imageColor;
    private bool _isFadeIn;
    private float _alpha;
    private float _timer;

    void Start()
    {
        _isFadeIn = true;
        _alpha = 0;
        _image = GetComponent<Image>();
        _imageColor = _image.color;
        _timer = 0;
    }

    void Update()
    {
        if (_isFadeIn)
        {
            _alpha += timing * Time.deltaTime;
            _imageColor.a = _alpha;
            _image.color = _imageColor;
            if (_alpha >= 1) _isFadeIn = false;
        }
        else
        {
            if (_timer < 1) _timer += timing * Time.deltaTime;
            else
            {
                _alpha -= timing * Time.deltaTime;
                _imageColor.a = _alpha;
                _image.color = _imageColor;
            }

            if (_alpha <= 0) Destroy(gameObject);
        }
    }
}
