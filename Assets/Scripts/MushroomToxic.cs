using UnityEngine;

public class MushroomToxic : Mushroom
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<GiantState>().ProcGiantState();
            Destroy(gameObject);
        }
    }
}
