using UnityEngine;

public class CameraManager : MonoBehaviour
{
    private Transform _playerTransform;

    private void Start() => _playerTransform = GameObject.Find("Player").transform;

    private void Update() => transform.position = new Vector3(_playerTransform.position.x, transform.position.y, transform.position.z);
}
