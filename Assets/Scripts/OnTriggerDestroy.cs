using UnityEngine;

public class OnTriggerDestroy : StateMachineBehaviour
{
    public override void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) => Destroy(animator.gameObject, stateInfo.length);
}
